package tests1;
import java.util.*;
/**
 * @author Alejandra
 * **/
public class Palindromo { // NOPMD by Alejandra on 30-04-17 23:25
	/***
	 *@author Alejandra 
	 ***/
	public static void main(final String[] args) {
		/*
		String frase1=pideFrase(); // NOPMD by Alejandra on 30-04-17 23:36
		frase1=quitaEspacios(frase1);
		final String frase2=vuelcaFrase(frase1);
		if(frase1.equalsIgnoreCase(frase2)){ // NOPMD by Alejandra on 30-04-17 23:36
			System.out.println("La frase es un palindromo.");  // NOPMD by Alejandra on 30-04-17 23:18
		}else{
			System.out.println("La frase NO es un palindromo.");  // NOPMD by Alejandra on 30-04-17 23:18
		}
		*/
	}
	public boolean check(String frase){
		boolean valido=false;
		String frase1=quitaEspacios(frase);
		final String frase2=vuelcaFrase(frase1);
		if(frase1.equalsIgnoreCase(frase2)){ // NOPMD by Alejandra on 30-04-17 23:36
			valido=true;
			System.out.println("La frase es un palindromo.");  // NOPMD by Alejandra on 30-04-17 23:18
		}else{
			System.out.println("La frase NO es un palindromo.");  // NOPMD by Alejandra on 30-04-17 23:18
		}
		return valido;
	}
	/**
	* 
	*
	* @author alejandra
	*/
	public String pideFrase(){
		final Scanner teclado=new Scanner(System.in);
		String frase=""; // NOPMD by Alejandra on 30-04-17 23:36
		do{
			System.out.print("Ingrese una frase: ");  // NOPMD by Alejandra on 30-04-17 23:18
			frase=teclado.nextLine(); // NOPMD by Alejandra on 30-04-17 23:36
			frase=frase.trim(); // NOPMD by Alejandra on 30-04-17 23:36
		}while(frase.length()==0); // NOPMD by Alejandra on 30-04-17 23:36
		teclado.close();
		return frase;
	}
/**
 * @author Alejandra
 * */
	public String quitaEspacios(final String finput){
		String frase=""; // NOPMD by Alejandra on 30-04-17 23:36
		char space;
		for(int i=0;i<finput.length();i++){
			final char car=finput.charAt(i);
			space=' ';
			if(car!=space){
				final String s_char=Character.toString(car);
				frase=frase.concat(s_char); // NOPMD by Alejandra on 30-04-17 23:36
			}
		}
		return frase;
	}
/**
 * @author Alejandra
 * **/
	public String vuelcaFrase(final String input){
		String out=""; // NOPMD by Alejandra on 30-04-17 23:36
		for (int i=0;i<input.length();i++) {
			final char car=input.charAt(i);
			out= Character.toString(car).concat(out); // NOPMD by Alejandra on 30-04-17 23:36
		}
		return out;
	}
}