import java.io.*;
import java.util.*;
public class Launcher{
	public static void main(String[] args) throws Exception {
		ProcessBuilder builder = new ProcessBuilder("java", "Palindromo");
		//builder.directory(new File("/home/fr4j4/temp/java_code")); //en caso de que se tenga que especificar una carpeta
		Process process = builder.start();

		OutputStream stdin = process.getOutputStream();
		InputStream stdout = process.getInputStream();

		BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

		writer.write("Amo la pacifica paloma");
		writer.flush();
		writer.close();

		Scanner scanner = new Scanner(stdout);
		while (scanner.hasNextLine()) {
			System.out.println(scanner.nextLine());
		}
	}
}